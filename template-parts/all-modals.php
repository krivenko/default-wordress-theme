
<!-- #modalCallback -->
<div class="modal fade" id="modalCallback" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            <div class="modal-title">
                <h4>Оставьте заявку</h4>
                <p>и мы свяжемся с вами в течении 5 минут</p>
            </div>
            <form class="common-form" data-toggle="validator" role="form" data-focus="false" novalidate="true">
                <div class="form-group">
                    <input type="text" class="form-control" name="name" placeholder="Имя">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="phone" placeholder="Телефон" required>
                </div>
                <button class="btn btn-primary submit"><span>Отправить</span></button>
                <div class="privacy">
                    <label class="checkbox">
                        <input type="checkbox" name="privacy" checked="">
                        <i></i>
                    </label>
                    <p>Я даю согласие на обработку персональных данных и соглашаюсь c <a href="<?php echo home_url('/privacy') ?>" target="_blank">политикой конфиденциальности</a></p>
                </div>
                <div class="info"></div>
                <input type="hidden" name="form_name" value="Форма в модали">
                <input type="hidden" name="modal_success" value="modalSuccess">
                <input type="hidden" name="ya_metrica_goal_name" value="">
                <input type="hidden" name="action" value="send_message">
                </form>
        </div>
    </div>
</div>

<!-- #modalSuccess -->
<div class="modal fade" id="modalSuccess" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            <div class="modal-title">
                <h4>Ваше сообщение успешно отправлено!</h4>
                <p>Мы свяжемся с вами в самое ближайшее время</p>
            </div>
        </div>
    </div>
</div>