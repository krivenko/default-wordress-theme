<?php get_header(); ?>

<section class="l-page page-404">
	<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-content">
                    <h1>Страница не найдена</h1>
                    <p>Попробуйте вернуться на <a href="<?php echo home_url('/') ?>">главную страницу</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>