//---------------------------------
//	Common
//---------------------------------

export function Common($) {

    // Lazy load image
    const lazyLoadAll = new LazyLoad({
        elements_selector: '.lazy'
    });

    

    // Куки
    // console.log($.cookie('_ga'))

    // Переход по якорям
    $('.scroll-to').click(function(e){
        const el = $(this).attr('href');

        if (window.location.href === globalParams.site_url + '/') {
            e.preventDefault();

            if (document.querySelector(el) !== null) {
                $('html, body').animate({
                    scrollTop: $(el).offset().top
                }, 500);
            }
        } else {
            window.location.href = globalParams.site_url + '/' + el;
        }
        
        return false;
    });

    // Baron scrollbar
    // $('.baron__clipper').baron({
    //     scroller: '.baron__clipper > .scroller',
    //     track: '.baron__clipper > .scroller__track',
    //     bar: '.baron__clipper > .scroller__track .scroller__bar',
    // });

}
